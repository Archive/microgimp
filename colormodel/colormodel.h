/*
 * Copyright (C) 1999 Jay Cox <jaycox@earthlink.net>
 * LGPL goes here
 */
#ifndef __COLOR_MODEL_H__
#define __COLOR_MODEL_H__

typedef (void*) PixelP;

typedef enum
{
  UNDEF_PRECISION,
  U8,
  U16,
  FLOAT;
} Precision;

typedef enum
{
  CUSTOM, XYZ, RGB, GRAY, INDEXED, CMYK; /* TODO: YIQ, HSV, HLS, HEXACHROME, etc. */
} ColorSpace;

typedef struct _ColorModel
{
  GimpObject  obj;              /* parent object */
/* Functions to be use mostly by the GUI */
  guint       is_additive;      /* bigger is brighter                        */
  guint       is_subtractive;   /* bigger is darker                          */
  const char**channel_names;    /* name of each channel's color              */
  const char *color_space_name; /* name of this color space                  */
  const char *alpha_string;     /* "Alpha" or "\0"                           */
  const char *precision_name;   /* precision string                          */
/* How the pixels are layed out in memory */
  Precision   precision;        /* pixel type                                */
  guint       bytes_per_channel;/* bytes per channel                         */
  guint       bytes;            /* bytes per pixel                           */
  guint       num_channels;     /* number of channels per pixel              */
  guint       num_colors;       /* number of channels that represent color   */
  guint       has_alpha;        /* pixel contains alpha                      */
  guint       alpha_channel;    /* channel number that holds alpha data      */
/*-- Generaly usefull info --*/
  ColorSpace  colorspace;       /* indexed, rgb, cmyk, etc.                  */
  guint       ID;               /* a unique id for quick compares            */
  guint       is_indexed;       /* pixel's color is from a lookup table      */
  guint       is_intensity;     /* pixel's color is computed from f(channels)*/
/*------*/
 /*  Some XYZ colorant stuff goes here? or does it go in the class? */
/*------*/
  /* derived objects hold icc profiles, indexed look uptables etc. */
  /* ColorProfileClass holds all the color transfer function pointers */
  /* derived classes implement the transfer functions */
}

typedef void (*ConvertFuncP) (ColorModel *, void *, ColorModel *, void *);

struct _ColorModelClass
{
  GimpObjectClass parent_class;
  void *first_func;
/* all pixel maniplators go here between first_func and last_func */
  /* the functions that go here twidle pixels */
  ConvertFuncP get_convert_func(ColorModel *cm);
/* -------------------------------------------------------------- */
  void *last_func;
};

#define COLOR_MODEL_CLASS(klass) \
  GTK_CHECK_CLASS_CAST (klass, color_model_get_type(), ColorModelClass)

#define COLOR_MODEL_TYPE  (color_model_get_type ())
#define COLOR_MODEL(obj)  (GIMP_CHECK_CAST ((obj), COLOR_MODEL_TYPE, ColorModel))
#define GIMP_IS_COLOR_MODEL(obj) (GIMP_CHECK_TYPE ((obj), COLOR_MODEL_TYPE))


#define    color_model_precision(c)         (c->precision)
#define    color_model_bytes_per_channel(c) (c->bytes_per_channel)
#define    color_model_bytes(c)             (c->bytes)
#define    color_model_is_indexed(c)        (c->is_indexed)
#define    color_model_is_intensity(c)      (c->is_indexed)
#define    color_model_num_channels(c)      (c->num_channels)
#define    color_model_num_colors(c)        (c->num_colors)
#define    color_model_has_alpha(c)         (c->has_alpha)
#define    color_model_alpha_channel(c)     (c->alpha_channel)

#define    color_model_alpha_string(c)      (c->alpha_string)
#define    color_model_precision_string(c)  (c->precision_string)

#define    color_model_is_additive(c)       (c->is_additive)
#define    color_model_is_subtractive(c)    (c->is_subtractive)

char *     color_model_string               (ColorModel *c);
char *     color_model_channel_string       (ColorModel *c, int channel);
char *     color_model_alpha_string         (ColorModel *c);
char *     color_model_color_space_string   (ColorModel *c);

#define    color_model_color_space(c)       (c->colorspace)
#define    color_model_format_ID(c)         (c->ID)

#define    color_model_has_func(c,func) ((COLOR_MODEL_CLASS(c)->func) ? TRUE : FALSE)
  
#define    color_model_get_func(c,func) ((COLOR_MODEL_CLASS(c)->func) ? COLOR_MODEL_CLASS(c)->func : g_warning("attempt to access unimplemented function $s in a colormodel", #func), NULL)

#endif /* __COLOR_PROFILE_H__ */
