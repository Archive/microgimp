/*
 * Copyright (C) 1999 Jay Cox <jaycox@earthlink.net>
 * LGPL goes here
 */

#include "colormodel.h"

static GimpObjectClass* parent_class;

static void
color_model_destroy(GimpObject *object)
{
  GIMP_OBJECT_CLASS(parent_class)->destroy (object);
}

static void
color_model_class_init (ColorModelClass *klass)
{
  GimpObjectClass *object_class;
  GimpType type;
  
  object_class = GIMP_OBJECT_CLASS(klass);

  parent_class = gimp_type_class(gimp_object_get_type());

  type = object_class->type;

  object_class->destroy = color_model_destroy;

/* zero out all the function pointers */
  memset(&klass->first_func, 0, &klass->last_func - &klass->first_func);
}

void
color_model_init(ColorModel *model)
{
  /* zero out all the variables. we should really do this by setting the 
     individual data members  */
  memset(&model->is_additive, 0,
         &model->is_intensity - &model->is_additive + sizeof(guint));
}

GimpType
color_model_get_type (void)
{
  static GimpType type = 0;
  if(!type)
    {
      static const GimpTypeInfo info = {
        "ColorModel",
        sizeof (ColorModel),
        sizeof (ColorModelClass),
        (GimpClassInitFunc) color_model_class_init,
        (GimpObjectInitFunc) color_model_init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GimpClassInitFunc) NULL
      };
    type = gimp_type_unique (gimp_object_get_type (), &info);
  }
  return type;
}

/* this class is pure virtual, there is no constructor for it (ie. color_model_new) */
