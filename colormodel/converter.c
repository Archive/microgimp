/*
 * Copyright (C) 1999 Jay Cox <jaycox@earthlink.net>
 * LGPL goes here
 */

static ColorModel *float_xyz; /* need to initialize these somewhere */
static ColorModel *float_xyz_a;

/*
 * The ColorModels MUST define a conversion to the xyz float format.
 *
 * They SHOULD define conversions to other colormodels which only differ
 * in precision or alpha channel.
 *
 * They should also define a fast conversion to 8 bit RGB for display
 */

/* TODO:
     Come up with a way to do any necessary precalculation for the conversion
     and also free the precalculation data when we are done.

   Probably something like this in the virtual class functions:
     void *color_model_convert_setup(destCM *cm); // returns an allocated object
     void  color_model_convert_finish(void *precalc_object); // frees the object
    also the ColorModelConvertFunc's should accept this object as an additional
    parameter.
*/
void
convert_pixels(ColorModel *destCM, void *dest,
	       ColorModel *sourceCM, void *source, length)
{
  ColorModelConvertFunc cmcf;
  cmcf = color_model_get_func(sourceCM, get_convert_func)(destCM);
  if (cmcf) /* we have a direct path for the conversion */
  {
    cmcf(destCM, dest, sourceCM, source, length);
    return;
  }
  else /* we have to use XYZ as an intermediary color space */
  {
    void *buf;
    ColorModel *tmpCM;
    if (color_model_has_alpha(destCM))
      tmpCM = float_xyz_a;
    else
      tmpCM = float_xyz;
    cmcf = color_model_get_func(sourcecm, get_convert_func)(tmpCM);
    if (cmcf == NULL)
    {
      g_warning("No path to XYZ which is needed to convert pixels\n");
      return;
    }
    buf = g_malloc(color_model_bytes(tmpCM)*length);
    cmcf(tmpCM, buf, sourceCM, source, length)
    cmcf = color_model_get_func(tmpCM, get_convert_func)(destCM);
    if (cmcf == NULL)
    {
      g_warning("No path from XYZ which is needed to convert pixels\n");
      g_free(buf);
      return;
    }
    cmcf(destCM, dest, tmpCM, buf, length);
    g_free(buf);
  }
}
