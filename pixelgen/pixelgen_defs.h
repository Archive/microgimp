#define U8_MULT(a,b)  ((t) = (a) * (b) + 0x80,   ((((t) >> 8)  + (t)) >> 8))
#define U16_MULT(a,b) ((t) = (a) * (b) + 0x8000, ((((t) >> 16) + (t)) >> 16))
#define FLOAT_MULT(a,b)  ((a) * (b))

#define U8_BLEND(a,b,alpha)    (U8_MULT   ((a)-(b), alpha) + (b))
#define U16_BLEND(a,b,alpha)   (U16_MULT  ((a)-(b), alpha) + (b))
#define FLOAT_BLEND(a,b,alpha) (FLOAT_MULT((a)-(b), alpha) + (b))

#define U8_DIV(a,b)  ((a) > (b)) ? U8_PIX_MAX  : \
                                   ((a)*(U8_PIX_MAX  + 1) / (1 + (b)))
#define U16_DIV(a,b) ((a) > (b)) ? U16_PIX_MAX : \
                                   ((a)*(U16_PIX_MAX + 1) / (1 + (b)))
#define FLOAT_DIV(a,b) ((a) > (b) && (b) != 0.0) ? FLOAT_PIX_MAX : ((a) / (b))

#define U8_ADD(a,b)    (((a) + (b) > U8_PIX_MAX)  ? U8_PIX_MAX  :  ((a) + (b)))
#define U16_ADD(a,b)   (((a) + (b) > U16_PIX_MAX) ? U16_PIX_MAX :  ((a) + (b)))
#define FLOAT_ADD(a,b) (((a) + (b) > FLOAT_PIX_MAX) ? FLOAT_PIX_MAX : \
                                                      ((a) + (b)))

#define U8_SUBTRACT(a,b)    (((b) > (a)) ? 0    : ((a) - (b)))
#define U16_SUBTRACT(a,b)   (((b) > (a)) ? 0    : ((a) - (b)))
#define FLOAT_SUBTRACT(a,b) (((b) > (a)) ? 0.0f : ((a) - (b)))

#define U8_CLAMP(a)    (((a) < 0) ? 0 : \
		        ((a) > U8_PIX_MAX) U8_PIX_MAX : U8_PIX_MAX : (a))
#define U16_CLAMP(a)   (((a) < 0) ? 0 : \
		        ((a) > U16_PIX_MAX) U16_PIX_MAX : U16_PIX_MAX : (a))
#define FLOAT_CLAMP(a) (((a) < 0.0f) ? 0.0f : \
			((a) > FLOAT_PIX_MAX) FLOAT_PIX_MAX : FLOAT_PIX_MAX : \
                                                              (a))

#define U8_PIX_MAX      255
#define U16_PIX_MAX     65535
#define FLOAT_PIX_MAX   1.0f

