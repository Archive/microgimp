#!/usr/bin/perl -w

# This is only a prototype of a code generation system.
# The input format of the final version will probably be much different.

# Copyright 1999 Jay Cox  <jaycox@earthlink.net>

sub modify_color_func {
    my ($statement, $precision, $cnum, $a1num, $a2num)=@_;
    my ($ac_start, $ac_end);
    # this precision->{accessor} stuff is only half done 
    # it is only needed for the float16 data type
    if (exists $precision->{accessor})
    {
	$ac_start = "$precision->{accessor}(";
	$ac_end   = ")";
    }
    else
    {
	$ac_start = "";
	$ac_end   = "";
    }

    $statement =~ s/src1/$ac_start src1[$cnum] $ac_end/g;
    $statement =~ s/src2/$ac_start src2[$cnum] $ac_end/g;

    $statement =~ s/PIXEL_MAX/$precision->{max}/g;
    $statement =~ s/MULT/$precision->{mult}/g;
    $statement =~ s/DIV/$precision->{div}/g;
    $statement =~ s/BLEND/$precision->{blend}/g;

    $statement =~ s/PIXEL/$precision->{type}/g;

    if ($a1num > 0) # we don't let channel 0 be the alpha channel?
    {
	$statement =~ s/alpha1/src1[$a1num]/g;
    }
    else
    {
	$statement =~ s/alpha1/$precision->{max}/g;
    }
    if ($a2num > 0) # we don't let channel 0 be the alpha channel?
    {
	$statement =~ s/alpha2/src2[$a2num]/g;
    }
    else
    {
	$statement =~ s/alpha2/$precision->{max}/g;
    }
    return $statement;
}

# currently unused
# do we even need a seperate function for the alpha computations?
sub modify_alpha_func {
    my ($statement, $precision, $cnum)=@_;
    $statement =~ s/src1/src1[$cnum]/g;
    $statement =~ s/src2/src2[$cnum]/g;
    return $statement;
}

sub print_func {
    my($struct, $precision, $model, $opt)=@_;
    my($struct1) = $struct;
    $struct1->{"name"} =~ s/PIXEL/$precision->{name}/g;
    $model->{perameters} =~ s/PIXEL/$precision->{type}/g;
    print "void ", $struct1->{name};
    print " (", $model->{perameters}, ")\n";
    print "{\n";
    print "  $precision->{longtype} t;\n";
    if ($opt->{unroll_loops} ne "yes")
    {
	print "  int b;\n";
	print "  int src1_stride, src2_stride;\n";
    }
    $model->{vars}     =~ s/PIXEL/$precision->{type}/g;
    $model->{pre_loop} =~ s/PIXEL/$precision->{type}/g;
    print "$model->{vars}";
    print "$model->{pre_loop}\n";

    if ($opt->{unroll_loops} eq "yes")
    {
	my ($i, $j, $foo, $alpha);
	my ($src1_stride, $src2_stride, $dest_stride);
	foreach(2, 1, 0)
	{
	    $alpha = $_;
	    print "  if (has_alpha1 && has_alpha2)\n" if ($alpha == 2);
	    print "  else if (has_alpha2)\n"           if ($alpha == 1);
	    print "  else\n"                           if ($alpha == 0);
	    print "  {\n";
	    print "    switch(ncolors)\n    {\n";
	    foreach(1, 3, 4)
	    {
		$i = $_;
		print "     case  $i:\n";
		print "      while (length--)\n";
		print "      {\n";
		for ($j = 0; $j < $i; $j++)
		{
		    $foo = modify_color_func($struct1->{color_comp},
					     $precision, $j, 1, 1);
		    print "        dest[$j] = $foo;\n";
		}
		if ($alpha == 2)
		{
		    $foo = modify_color_func($struct1->{alpha_comp},
					     $precision, $i, 1, 1);
		    print "        dest[$i] = $foo;\n";
		}
		elsif ($alpha == 1)
		{
		    print "        dest[$i] = src2[$i];\n";
		}
		$src1_stride = $i;
		$src2_stride = $i;
		$src1_stride = $i + 1 if ($alpha == 2);
		$src2_stride = $i + 1 if ($alpha >= 1);
		$dest_stride = $src2_stride;
		print "        src1 += $src1_stride;\n";
		print "        src2 += $src2_stride;\n";
		print "        dest += $dest_stride;\n";
		print "      } break;\n";
	    }
	    print "    }\n";
	    print "  }\n";
	}
    }
    else
    {
	my ($foo);
	print "  src1_stride = ncolors + has_alpha1;\n";
	print "  src2_stride = ncolors + has_alpha2;\n";
	print "\n";
	print "  while (length--)\n";
	print "  {\n";
	print "    for (b = 0; b < ncolors; b++)\n";
	$foo = modify_color_func($struct1->{color_comp},
				 $precision, "b", 1, 1);
	print "      dest[b] = $foo;\n";
	$foo = modify_color_func($struct1->{alpha_comp},
				 $precision, "b", 1, 1);
	print "    if (has_alpha2)\n";
	print "      if (has_alpha1)\n";
	print "        dest[b] = $foo;\n";
	print "      else\n";
	print "        dest[b] = src2[b];\n";
	print "    src1 += src1_stride;\n";
	print "    src2 += src2_stride;\n";
	print "    dest += src2_stride;\n";
	print "  }\n";
    }

	
    print "}\n";
}

sub print_help {
    print <<END_HELP;

Usage: pixelgen01.pl function_type {multiply, divide, add, subtract,
                                    difference, screen, overlay,
                                    darken, lighten}
                     precision_type {int8, int16, float}
                     model_type {gimp, hollywood}
                     unroll_loops {yes, no}

END_HELP

    exit (1);
}


{  
    my ($func_type, $precision_type, $model_type, $unroll) = @ARGV;
    $multiply_func = {
	name => 'multiply_pixels_PIXEL', 
	color_comp => 'MULT(src1, src2)',
	alpha_comp => 'MIN (src1, src2)'
	};
    $divide_func = {
	name => 'divide_pixels_PIXEL', 
	color_comp => 'DIV (src1, src2)',
	alpha_comp => 'MIN (src1, src2)'
	};
    $screen_func = {
	name => 'screen_pixels_PIXEL', 
	color_comp => 'PIXEL_MAX - MULT((PIXEL_MAX - src1), (PIXEL_MAX - src2))',
	alpha_comp => 'MIN (src1, src2)'
	};
    $overlay_func = {
	name => 'overlay_pixels_PIXEL', 
	color_comp => 'MULT(src1, src1 + MULT(2*src2, PIXEL_MAX - src1))',
	alpha_comp => 'MIN (src1, src2)'
	};
    $add_func = {
	name => 'add_pixels_PIXEL', 
	color_comp => 'MIN (PIXEL_MAX, src1 + src2)',
	alpha_comp => 'MIN (src1, src2)'
	};
    $subtract_func = {
	name => 'subtract_pixels_PIXEL', 
	color_comp => 'MAX (0, src1 - src2)',
	alpha_comp => 'MIN (src1, src2)'
	};
    $difference_func = {
	name => 'difference_pixels_PIXEL', 
	color_comp => '(src1 > src2) ? src1 - src2 : src2 - src1',
	alpha_comp => 'MIN (src1, src2)'
	};
    $darken_func = {
	name => 'darken_pixels_PIXEL', 
	color_comp => 'MIN (src1, src2)',
	alpha_comp => 'MIN (src1, src2)'
	};
    $lighten_func = {
	name => 'lighten_pixels_PIXEL', 
	color_comp => 'MAX (src1, src2)',
	alpha_comp => 'MIN (src1, src2)'
	};
    $int8_precision = {
	name => 'u8',
	type => 'guint8',
	longtype => 'long',
	max   => 'U8_PIX_MAX',
	mult  => 'U8_MULT',
	div   => 'U8_DIV',
	blend => 'U8_BLEND',
	};
    $int16_precision = {
	name => 'u16',
	type => 'guint16',
	longtype => 'long',
	max   => 'U16_PIX_MAX',
	mult  => 'U16_MULT',
	div   => 'U16_DIV',
	blend => 'U16_BLEND',
	};
    $float_precision = {
	name => 'float',
	type => 'float',
	longtype => 'double',
	max   => 'FLOAT_PIX_MAX',
	mult  => 'FLOAT_MULT',
	div   => 'FLOAT_DIV',
	blend => 'FLOAT_BLEND',
	};
    $gimp_model = {
	perameters => <<END,
	const PIXEL *src1, const PIXEL *src2,
	PIXEL *dest, int length, int ncolors,
	int has_alpha1, int has_alpha2
END
        vars => "",
        pre_loop => "",
        };
    $hollywood_model = {
	perameters => 'PixelRow *src1_row, PixelRow *src2_row, PixelRow *dest_row',
        vars => <<END_VARS,
  Tag src1_tag   = pixelrow_tag (src1_row);
  Tag src2_tag   = pixelrow_tag (src2_row);
  int has_alpha1 = (tag_alpha (src1_tag)==ALPHA_YES)? 1: 0;
  int has_alpha2 = (tag_alpha (src2_tag)==ALPHA_YES)? 1: 0;
  PIXEL *src1    = (PIXEL*) pixelrow_data (src1_row);
  PIXEL *src2    = (PIXEL*) pixelrow_data (src2_row);
  PIXEL *dest    = (PIXEL*) pixelrow_data (dest_row);
  int length     = pixelrow_width (dest_row);
  int ncolors    = tag_num_channels (src2_tag) - (has_alpha2) ? 1 : 0;
END_VARS
        pre_loop => ''
        };
    $opt = {
	unroll_loops => "no"
	};
    $func_map = {
	multiply   => $multiply_func,
	divide     => $divide_func,
	screen     => $screen_func,
	overlay    => $overlay_func,
	add        => $add_func,
	subtract   => $subtract_func,
	difference => $difference_func,
	darken     => $darken_func,
	lighten    => $lighten_func
	};
    unless (exists $func_map->{$func_type})
    {
	print qq/bad function type. Must be "multiply", "divide", "add", "subtract", "difference", "screen", "overlay", "darken", or "lighten"\n/;
	print_help();
    }
    unless ($precision_type eq "int8" || $precision_type eq "float" ||
	     $precision_type eq "int16")
    {
	print qq/bad precision. Must be "int8", "int16" or "float".\n/;
	print_help();
    }
    unless ($model_type eq "gimp" || $model_type eq "hollywood")
    {
	print qq/bad model type.  Must be "gimp" or "hollywood".\n/;
	print_help();
    }
    unless ($unroll eq "yes" || $unroll eq "no")
    {
	print qq/bad unroll value.  Must be "yes" or "no".\n/;
	print_help();
    }
    if ($precision_type eq "int8")  { $precision = $int8_precision;  }
    if ($precision_type eq "int16") { $precision = $int16_precision; }
    if ($precision_type eq "float") { $precision = $float_precision; }
    if ($model_type eq "gimp")      { $model = $gimp_model;          }
    if ($model_type eq "hollywood") { $model = $hollywood_model;     }

    if ($unroll eq "yes") { $opt->{unroll_loops} = "yes"; }

    $func = $func_map->{$func_type};

    print <<END_HEADER;
/* This is a generated file.  Do not edit */

#include <glib.h>

#define U8_MULT(a,b)  ((t) = (a) * (b) + 0x80,   ((((t) >> 8)  + (t)) >> 8))
#define U16_MULT(a,b) ((t) = (a) * (b) + 0x8000, ((((t) >> 16) + (t)) >> 16))
#define FLOAT_MULT(a,b)  ((a) * (b))

#define U8_BLEND(a,b,alpha)    (U8_MULT   ((a)-(b), alpha) + (b))
#define U16_BLEND(a,b,alpha)   (U16_MULT  ((a)-(b), alpha) + (b))
#define FLOAT_BLEND(a,b,alpha) (FLOAT_MULT((a)-(b), alpha) + (b))

#define U8_DIV(a,b)  (a > b) ? U8_PIX_MAX :  (a*(U8_PIX_MAX  + 1) / (1 + b))
#define U16_DIV(a,b) (a > b) ? U16_PIX_MAX : (a*(U16_PIX_MAX + 1) / (1 + b))
#define FLOAT_DIV(a,b) (a > b && b != 0.0) ? FLOAT_PIX_MAX : (a / b)



#define U8_PIX_MAX      255
#define U16_PIX_MAX     65535
#define FLOAT_PIX_MAX   1.0f

END_HEADER

    print_func ($func, $precision, $model, $opt);
}
