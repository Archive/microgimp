#!/usr/bin/perl -w

# Copyright 1999-2000 Jay Cox  <jaycox@earthlink.net>

#this doesn't even come close to working yet

$colormodels = {
		"BW "     => {
			      hierarchy => ["bw", "additive", "intensity"],
			      ncolors => 1,
			      channels => ["value"]
			     },
		"RGB"     => {
			      hierarchy => ["rgb", "additive", "intensity"],
			      ncolors => 3,
			      channels => ["red", "green", "blue"]
			     },
			       
		"XYZ"     => {
			      hierarchy => ["xyz", "additive", "intensity"],
			      ncolors => 3,
			      channels => ["x", "y", "z"]
			     },
		"CMYK"    => {
			      hierarchy => ["cmyk", "subtractive",
					    "intensity"],
			      ncolors => 4,
			      channels => ["cyan", "magenta", "yellow",
					   "black"]
			     }
		"INDEXED" => {
			      hierarchy => ["indexed"],
			      ncolors => 1,
			      channels => ["indexed"]
			     }
	     };

$int8_precision =  {
		    name     => 'u8',
		    type     => 'guint8',
		    longtype => 'long',
		    max      => 'U8_PIX_MAX',
		    mult     => 'U8_MULT',
		    div      => 'U8_DIV',
		    blend    => 'U8_BLEND',
		    add      => 'U8_ADD',
		    subtract => 'U8_SUBTRACT',
		    clamp    => 'U8_CLAMP',
		   };
$int16_precision = {
		    name     => 'u16',
		    type     => 'guint16',
		    longtype => 'long',
		    max      => 'U16_PIX_MAX',
		    mult     => 'U16_MULT',
		    div      => 'U16_DIV',
		    blend    => 'U16_BLEND',
		    add      => 'U16_ADD',
		    subtract => 'U16_SUBTRACT',
		    clamp    => 'U16_CLAMP',
		   };
$float_precision = {
		    name     => 'float',
		    type     => 'float',
		    longtype => 'double',
		    max      => 'FLOAT_PIX_MAX',
		    mult     => 'FLOAT_MULT',
		    div      => 'FLOAT_DIV',
		    blend    => 'FLOAT_BLEND',
		    add      => 'FLOAT_ADD',
		    subtract => 'FLOAT_SUBTRACT',
		    clamp    => 'FLOAT_CLAMP',
		   };

sub modify_color_func {
    my ($statement, $precision, $cnum, $a1num, $a2num)=@_;
    my ($ac_start, $ac_end);

    $statement =~ s/src1/$ac_start src1[$cnum] $ac_end/g;
    $statement =~ s/src2/$ac_start src2[$cnum] $ac_end/g;

    $statement =~ s/PIXEL_MAX/$precision->{max}/g;
    $statement =~ s/MULT/$precision->{mult}/g;
    $statement =~ s/DIV/$precision->{div}/g;
    $statement =~ s/BLEND/$precision->{blend}/g;
    $statement =~ s/ADD/$precision->{add}/g;
    $statement =~ s/SUBTRACT/$precision->{subtract}/g;
    $statement =~ s/CLAMP/$precision->{clamp}/g;

    $statement =~ s/PIXEL/$precision->{type}/g;
    $statement =~ s/LONGPIXEL/$precision->{longtype}/g;

    if ($a1num > 0) # we don't let channel 0 be the alpha channel?
    {
	$statement =~ s/alpha1/src1[$a1num]/g;
    }
    else
    {
	$statement =~ s/alpha1/$precision->{max}/g;
    }
    if ($a2num > 0) # we don't let channel 0 be the alpha channel?
    {
	$statement =~ s/alpha2/src2[$a2num]/g;
    }
    else
    {
	$statement =~ s/alpha2/$precision->{max}/g;
    }
    return $statement;
}

# currently unused
# do we even need a seperate function for the alpha computations?
sub modify_alpha_func {
    my ($statement, $precision, $cnum)=@_;
    $statement =~ s/src1/src1[$cnum]/g;
    $statement =~ s/src2/src2[$cnum]/g;
    return $statement;
}

sub print_func {
    my($struct, $precision, $model, $opt)=@_;
    my($struct1) = $struct;
    $struct1->{"name"} =~ s/PIXEL/$precision->{name}/g;
    $model->{parameters} =~ s/PIXEL/$precision->{type}/g;
    print "void ", $struct1->{name};
    print " (", $model->{parameters}, ")\n";
    print "{\n";
    print "  $precision->{longtype} t;\n";
    if ($opt->{unroll_loops} ne "yes")
    {
	print "  int b;\n";
	print "  int src1_stride, src2_stride;\n";
    }
    $model->{vars}     =~ s/PIXEL/$precision->{type}/g;
    $model->{pre_loop} =~ s/PIXEL/$precision->{type}/g;
    print "$model->{vars}";
    print "$model->{pre_loop}\n";

    if ($opt->{unroll_loops} eq "yes")
    {
	my ($i, $j, $foo, $alpha);
	my ($src1_stride, $src2_stride, $dest_stride);
	foreach(2, 1, 0)
	{
	    $alpha = $_;
	    print "  if (has_alpha1 && has_alpha2)\n" if ($alpha == 2);
	    print "  else if (has_alpha2)\n"           if ($alpha == 1);
	    print "  else\n"                           if ($alpha == 0);
	    print "  {\n";
	    print "    switch(ncolors)\n    {\n";
	    foreach(1, 3, 4)
	    {
		$i = $_;
		print "     case  $i:\n";
		print "      while (length--)\n";
		print "      {\n";
		for ($j = 0; $j < $i; $j++)
		{
		    $foo = modify_color_func($struct1->{color_comp},
					     $precision, $j, 1, 1);
		    print "        dest[$j] = $foo;\n";
		}
		if ($alpha == 2)
		{
		    $foo = modify_color_func($struct1->{alpha_comp},
					     $precision, $i, 1, 1);
		    print "        dest[$i] = $foo;\n";
		}
		elsif ($alpha == 1)
		{
		    print "        dest[$i] = src2[$i];\n";
		}
		$src1_stride = $i;
		$src2_stride = $i;
		$src1_stride = $i + 1 if ($alpha == 2);
		$src2_stride = $i + 1 if ($alpha >= 1);
		$dest_stride = $src2_stride;
		print "        src1 += $src1_stride;\n";
		print "        src2 += $src2_stride;\n";
		print "        dest += $dest_stride;\n";
		print "      } break;\n";
	    }
	    print "    }\n";
	    print "  }\n";
	}
    }
    else
    {
	my ($foo);
	print "  src1_stride = ncolors + has_alpha1;\n";
	print "  src2_stride = ncolors + has_alpha2;\n";
	print "\n";
	print "  while (length--)\n";
	print "  {\n";
	print "    for (b = 0; b < ncolors; b++)\n";
	$foo = modify_color_func($struct1->{color_comp},
				 $precision, "b", 1, 1);
	print "      dest[b] = $foo;\n";
	$foo = modify_color_func($struct1->{alpha_comp},
				 $precision, "b", 1, 1);
	print "    if (has_alpha2)\n";
	print "      if (has_alpha1)\n";
	print "        dest[b] = $foo;\n";
	print "      else\n";
	print "        dest[b] = src2[b];\n";
	print "    src1 += src1_stride;\n";
	print "    src2 += src2_stride;\n";
	print "    dest += src2_stride;\n";
	print "  }\n";
    }

	
    print "}\n";
}

sub print_help {
    print <<END_HELP;

Usage: pixelgen-gegl.pl [generaton flags go here] file.pgen

END_HELP

    exit (1);
}


{  

    $hollywood_model = {
	parameters => 'PixelRow *src1_row, PixelRow *src2_row, PixelRow *dest_row',
        vars => <<END_VARS,
  Tag src1_tag   = pixelrow_tag (src1_row);
  Tag src2_tag   = pixelrow_tag (src2_row);
  int has_alpha1 = (tag_alpha (src1_tag)==ALPHA_YES)? 1: 0;
  int has_alpha2 = (tag_alpha (src2_tag)==ALPHA_YES)? 1: 0;
  PIXEL *src1    = (PIXEL*) pixelrow_data (src1_row);
  PIXEL *src2    = (PIXEL*) pixelrow_data (src2_row);
  PIXEL *dest    = (PIXEL*) pixelrow_data (dest_row);
  int length     = pixelrow_width (dest_row);
  int ncolors    = tag_num_channels (src2_tag) - (has_alpha2) ? 1 : 0;
END_VARS
        pre_loop => ''
        };
    $opt = {
	unroll_loops => "no"
	};
    unless ($precision_type eq "int8" || $precision_type eq "float" ||
	     $precision_type eq "int16")
    {
	print qq/bad precision. Must be "int8", "int16" or "float".\n/;
	print_help();
    }

    print_func ($func, $precision, $model, $opt);
}
